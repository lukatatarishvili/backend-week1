<?php  require_once 'header.php';?>

<body>
<div class="container" style="padding-top:30px;">
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Product name</th>
      <th scope="col">Description</th>
    </tr>
  </thead>
  <tbody>
  <?php 
    require_once 'db.php';
    $sql="SELECT * FROM `products` order by id desc";
    $result = $mysqli->query($sql);
    while ($row = $result->fetch_assoc())  { ?>
      <tr>
        <th scope="row"><?=$row['id']?></th>
        <th scope="row"><?=$row['product_name']?></th>
        <th scope="row"><?=$row['description']?></th>
        
    </tr>
   <?php } 
  ?>
 
    
  </tbody>
</table>
</div>
</body>
</html>