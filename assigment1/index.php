<?php  require_once 'header.php';?>

<!-- session alerts -->
<?php 
	 if (isset($_SESSION['message'])): ?>
	 	    <div class="alert alert-<?=$_SESSION['msg_type']?> alert-dismissible fade show" role="alert">
        <?php 
                echo $_SESSION['message'];
                unset($_SESSION['message']); 
        ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
	 
	 	    </div>	
	 	<?php endif; ?>
<!-- end session alerts -->

<!-- insert records start -->
	<div class="row justify-content-center" style="padding-top:30px;">
		<form action="process.php" method="POST" >
		
			<div class="form-group">
			    <label>Product name</label>
			    <input type="text" name="product_name" required class="form-control" placeholder="Product name">
			</div>
			<div class="form-group">
			    <label>Description</label>
			    <input type="text" name="description" required class="form-control" placeholder="Description">
			</div>
			<div class="form-group">
				
				<button type="submit" class="btn btn-success"   name="save">Save </button>
				
			</div>
<!-- insert records end -->

</body>
</html>